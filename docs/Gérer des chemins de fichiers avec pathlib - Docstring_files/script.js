$(document).ready(function () {
    $("#buy-button").click(startCheckout);
    $("#club-subscribe-button").click(startSubscribe);
    $('.update-shopping-cart').on("click", updateShoppingCart);
});

// CONSTANTS //
window.dark_mode_const = -1;

// COPY CODE TO CLIPBOARD
function copyStringWithNewLineToClipBoard(stringWithNewLines) {
    const temp_textarea = document.createElement('textarea');
    temp_textarea.innerHTML = stringWithNewLines;
    const parentElement = document.getElementById('textarea-to-copy-text-to-clipboard');
    parentElement.appendChild(temp_textarea);
    temp_textarea.select();
    document.execCommand('copy');
    parentElement.removeChild(temp_textarea);
}

$(".copy-code").click(function () {
    var code_to_copy = $(this).parent().find(".code-to-copy");
    copyStringWithNewLineToClipBoard(code_to_copy.text());
    toastr.options.timeOut = 1000;
    toastr.success('&nbsp; &nbsp; Copié dans le presse-papier');
});


function copy_html(image_url) {
    copyStringWithNewLineToClipBoard(image_url);
    toastr.options.timeOut = 1000;
    toastr.success('&nbsp; &nbsp; Copié dans le presse-papier');
}

// FAQ SEARCH
$(function () {
    function FullTextContains(innerText, searchTerm) {
        for (x = 0; x < searchTerm.length; x++) {
            if (innerText.toLowerCase().indexOf(searchTerm[x].toLowerCase()) >= 0) {
                return true;
            }
        }
        return false;
    }

    $('.faq').on('keyup input', function (e) {
        var text = $.trim($(this).val());
        if (e.keyCode == 13) {
            SearchFAQ(text);
        } else if (text == '') {
            SearchFAQ('');
        }
    });

    $('#btnFaqSearch').on('click', function (e) {
        var text = $.trim($('.faq').val());
        SearchFAQ(text);
    });

    function SearchFAQ(searchTermText) {
        var searchTerm = searchTermText.split(' ');
        if (searchTermText != '') {
            $(".faq-list .panel").filter(function (index) {
                var panelText = $.trim($(this).text());
                if (FullTextContains(panelText, searchTerm) == true) {
                    return true;
                } else {
                    $(this).slideUp();
                    return false;
                }
            }).slideDown();
        } else {
            $(".faq-list .panel").slideDown();
        }
    }
});

// DARK MODE TOGGLE
function darkMode() {
    var post_body = document.getElementById("post-body");
    var dark_mode_toggle = document.getElementById("dark-mode-toggle");
    window.dark_mode_const = -window.dark_mode_const;
    post_body.classList.toggle("bg-really-dark");
    post_body.classList.toggle("text-light");
    dark_mode_toggle.classList.toggle("fe-sun");
    dark_mode_toggle.classList.toggle("fe-moon");

    var elements = document.getElementsByTagName("a");
    for (var i = 0; i < elements.length; i++) {

        if (window.dark_mode_const == 1) {
            elements[i].style.color = "#a1b3ed";
        } else {
            elements[i].style.color = "#335eea";
        }

    }

}

// CHANGE SECTION'S ICON
function changeSectionIcon(section) {

    var section_icon = section.getElementsByClassName("section-icon")[0];
    section_icon.classList.toggle("fe-folder-plus")
    section_icon.classList.toggle("fe-folder-minus")

}

// QUIZZ QUESTION TOGGLE
function toggle_question(toggle) {
    var qsup = document.getElementById('q-sup');
    if (toggle == 'yes') {
        qsup.classList.remove("collapse");
    } else {
        qsup.classList.add("collapse");
    }
}

function toggle_question2(toggle) {
    var qsup = document.getElementById('q-sup2');
    if (toggle == 'yes') {
        qsup.classList.remove("collapse");
    } else {
        qsup.classList.add("collapse");
    }
}

// GET AJAX FORM COOKIE
$(function () {
    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

});

// CHANGE CLUB PRICE
function changePrice() {
    var trim = $("#billingSwitch").prop('checked');
    if (trim) {
        $(".price-amount").each(function test() {
            current_price = parseInt($(this).text());
            if (current_price == 49) {
                $(this).html(current_price + 80);
            } else {
                $(this).html(current_price + 120);
            }
            ;
        })
    } else {
        $(".price-amount").each(function test() {
            current_price = parseInt($(this).text());
            if (current_price == 129) {
                $(this).html(current_price - 80);
            } else {
                $(this).html(current_price - 120);
            }
            ;
        })
    }

}

// CHANGE PROFILE PIC
function readURL(input, img_id) {
    if (input.files && input.files[0]) {
        if (input.files[0].size > 2000000) {
            $("#img-too-big-" + img_id).removeClass('d-none');
        } else {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#' + img_id).attr('src', e.target.result);
                $("#img-too-big-" + img_id).addClass('d-none');
            };

            reader.readAsDataURL(input.files[0]);

        }
    }
}

function updateImageUploadName(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[0]);
        var image_label = document.querySelector("#image-upload-label");
        image_label.innerHTML = input.files[0].name;
    }
}

// CREDIT CARD FORM TOGGLE
$("input[name='use-registered-card']").change(function () {
    var n = $("#customRadio2");
    var use_new_card = $('#new-card');
    var existing_card = $('#existing-card');
    if (n.is(':checked')) {
        use_new_card.show();
        existing_card.hide();
    } else {
        use_new_card.hide();
        existing_card.show();
    }

});


// FORMS VALIDATION
function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}


$(document).ready(function () {
    if ($('#email')) {
        $('#email').keyup(delay(function (e) {
            validateEmail();
        }, 250));
    }

    if ($('#password')) {
        $('#password').keyup(delay(function (e) {
            validatePassword();
        }, 250));
    }

});

function validateEmailString(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePassword() {
    var field_val = $("#password").val();
    if (field_val.length < 8) {
        $("#password").addClass("is-invalid");
        $("#password").removeClass("is-valid");
        $("#password-validation-warning").removeClass("d-none");
        $("#password-validation-icon").removeClass("d-none");
        $("#password-validation-icon").removeClass("fe-check-circle");
        $("#password-validation-icon").addClass("fe-alert-circle");
        $("#password-validation-icon").addClass("text-danger");
        $("#password-validation-icon").removeClass("text-success");
        $('#password-validation-icon').attr('title', "Trop court");
        $('#password-validation-icon').attr('data-content', "Votre mot de passe doit contenir au moins 8 caractères.");
        $('#password-validation-icon').popover("enable");
    } else {
        $("#password").addClass("is-valid");
        $("#password").removeClass("is-invalid");
        $("#password-validation-warning").addClass("d-none");
        $("#password-validation-icon").removeClass("text-danger");
        $("#password-validation-icon").addClass("text-success");
        $("#password-validation-icon").addClass("fe-check-circle");
        $("#password-validation-icon").removeClass("fe-alert-circle");
        $("#password-validation-icon").popover("disable");
    }
}

function validateEmail() {
    var email = $("#email").val();
    if (!validateEmailString(email)) {
        $("#email").addClass("is-invalid");
        $("#email").removeClass("is-valid");
        $("#email-validation-icon").removeClass("d-none");
        $("#email-validation-icon").removeClass("fe-check-circle");
        $("#email-validation-icon").addClass("fe-alert-circle");
        $("#email-validation-icon").addClass("text-danger");
        $("#email-validation-icon").removeClass("text-success");
        $('#email-validation-icon').attr('title', "E-mail invalide");
        $('#email-validation-icon').attr('data-content', "Adresse e-mail invalide.");
        $('#email-validation-icon').popover("enable");
    } else {
        $("#email").addClass("is-valid");
        $("#email").removeClass("is-invalid");
        $("#email-error").text("");
        $("#email-error").removeClass("text-danger");
        $("#email-error").addClass("text-success");
        $("#email-validation-icon").removeClass("text-danger");
        $("#email-validation-icon").addClass("text-success");
        $("#email-validation-icon").addClass("fe-check-circle");
        $("#email-validation-icon").removeClass("fe-alert-circle");
        $('#email-validation-icon').popover("disable");
    }
}

function validateSignup() {
    var prenom = $("#prenom").val();
    var email = $("#email").val();
    var password = $("#password").val();
    if (prenom.length < 3) {

        $('#prenom-validation-icon').popover("show");
        return false;

    } else if (!validateEmailString(email)) {

        $('#email-validation-icon').popover("show");
        return false;

    } else if (password.length < 8) {

        $('#password-validation-icon').popover("show");
        return false;

    }
    return true;
}

// BLOG


$("#blog_content").scroll(function () {
    $("#blog_result").scrollTop($("#blog_content").scrollTop());
    $("#blog_result").scrollLeft($("#blog_content").scrollLeft());
});
$("#blog_result").scroll(function () {
    $("#blog_content").scrollTop($("#blog_result").scrollTop());
    $("#blog_content").scrollLeft($("#blog_result").scrollLeft());
});


// SCROLL TO ACTIVE SECTION
function scrollToSectionCurrent(behavior) {
    var toc = document.getElementById("sidebar");
    var section = document.getElementsByClassName('section-current');
    if (section.length > 0) {
        var section = section[0];
        var posTop = section.offsetTop;
        var height = section.offsetHeight / 2;
        toc.scroll({
            top: posTop - height - 80,
            behavior: behavior
        });
    }
}

function toggleSectionCurrent(el) {
    var div = el.parentNode;
    // Remove section-current from current section
    var section = document.getElementsByClassName('section-current');
    if (section.length > 0) {
        var section = section[0];
        section.classList.remove("section-current")
    }

    div.classList.add('section-current');
    scrollToSectionCurrent('smooth');
}

function openSection(el) {
    var folder_icon = $(el).find("i");
    folder_icon.toggleClass('fe-chevron-right fe-chevron-down');
}

function expandAll(el) {
    if ($(el).text() === "Développer tout") {
        var collapse_all = false;
        $(el).text("Réduire tout");
    } else {
        var collapse_all = true;
        $(el).text("Développer tout");
    }

    $('.session-main-div').each(function () {
        var folder_icon = $(this).parent().find(".section-folder-icon");

        if (collapse_all) {
            $(this).removeClass("show");
            folder_icon.addClass('fe-chevron-right');
            folder_icon.removeClass('fe-chevron-down');
        } else {
            $(this).addClass("show");
            folder_icon.removeClass('fe-chevron-right');
            folder_icon.addClass('fe-chevron-down');
        }


    })
}


function openTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
    sessionStorage.setItem("default_tab", tabName);
}


(function ($) {
    "use strict";
    var fullHeight = function () {
        $('.js-fullheight').css('height', $(window).height());
        $(window).resize(function () {
            $('.js-fullheight').css('height', $(window).height());
        });
    };
    fullHeight();
})(jQuery);


$(document).ready(function () {
    var navbar_hidden = sessionStorage.getItem("navbar_hidden");
    var sidebar = document.getElementById("sidebar");
    if (sidebar) {
        if (navbar_hidden === 'true') {
            sidebar.style.width = "0px";
            document.getElementById("openNavBar").classList.remove("d-none");
        } else {
            document.getElementById("openNavBar").classList.add("d-none");
            document.getElementById("sidebar").style.width = "600px";
        }
    }

    $('input[required]').on('invalid', function () {
        this.setCustomValidity($(this).data("required-message"));
    });

    var default_open = sessionStorage.getItem("default_tab");

    if (default_open) {
        var tab_to_click = document.getElementById("btn-" + default_open);
    } else {
        var tab_to_click = document.getElementById("btn-Progression");
    }

    if (tab_to_click) {
        tab_to_click.click();
    }

    // Setup playback speed
    var pb_speed_storage = sessionStorage.getItem("playback_speeds");
    var pb_speed_text = document.getElementById("playback-speed");
    if (pb_speed_text) {
        pb_speed_text.innerHTML = pb_speed_storage || '1.0';
    }

    scrollToSectionCurrent('auto');
    setup_vimeo_autoplay();
});


function setup_vimeo_autoplay() {
    var iframes = document.querySelectorAll('iframe');
    const autoplay = document.querySelector("#autoplay");

    if (!autoplay) {
        return;
    }

    var pb_speed = parseFloat(sessionStorage.getItem("playback_speed"));
    const pb_rate = pb_speed || 1;

    if (iframes) {
        if (autoplay.checked) {
            for (let [index, iframe] of iframes.entries()) {
                var player = new Vimeo.Player(iframe);
                if (autoplay.checked && index === 0) {
                    player.setPlaybackRate(pb_rate);
                    player.play();
                }
            }
        }
        for (let [index, iframe] of iframes.entries()) {
            let player = new Vimeo.Player(iframe);
            player.on('play', function (event) {
                let pb_speed = parseFloat(sessionStorage.getItem("playback_speed")) || 1;
                this.setPlaybackRate(pb_speed);
            })
            if (index === 0) {
                player.on('timeupdate', function (event) {
                    //  Set session as completed.
                    if (event.percent >= 0.9) {
                        player.off('timeupdate');
                        var form = $("#complete-session-form");
                        if (form) {
                            set_session_as_completed(form.attr('action'), true);
                            var form_div = form.children('div');
                            if (form_div.length === 1) {
                                checkark_div = form_div[0];
                                checkark_div.classList.add("checked");
                            }
                        }
                    }
                });
            }
            player.on('ended', function (event) {
                // Go to next page.
                let session_type = document.querySelector("#session-type");
                if (session_type.value !== "Exercice") {
                    navigate_session('next');
                }
            });


        }
    }
}


function toggleFullScreen(state) {

    let player = document.querySelector("#player-frame");
    if (!player) {
        return;
    }
    let footer = document.getElementsByTagName("footer")[0];

    if (state === 'on') {
        document.body.style.overflow = "hidden";
        footer.style.zIndex = -1;
        let player = document.querySelector("#player-frame");
        player.classList.add('lesson-player-fullscreen');

        let full_screen_btn = document.querySelector("#exit-fullscreen");
        full_screen_btn.classList.remove('d-none');

        const frames = ['statistics', 'edit-session', 'table-of-content', 'main-header', 'tabs']

        for (let frame of frames) {
            let frame_element = document.querySelector("#" + frame);
            if (frame_element) {
                frame_element.style.zIndex = 0;
            }
        }
        sessionStorage.setItem("fullscreen_state", true)
    } else {
        document.body.style = "";
        footer.style.zIndex = "";
        let player = document.querySelector("#player-frame");
        let full_screen_btn = document.querySelector("#exit-fullscreen");

        player.classList.remove('lesson-player-fullscreen');
        full_screen_btn.classList.add('d-none');

        const frames = ['statistics', 'edit-session', 'table-of-content', 'main-header', 'tabs']

        for (let frame of frames) {
            let frame_element = document.querySelector("#" + frame);
            if (frame_element) {
                frame_element.style.zIndex = "auto";
            }
        }
        sessionStorage.setItem("fullscreen_state", false)
    }
}


function setPlaybackSpeed(way) {

    var iframes = document.querySelectorAll('iframe');
    var current_pb_rate = sessionStorage.getItem("playback_speed");
    current_pb_rate = (Math.round(parseFloat(current_pb_rate) * 100) / 100) || 1;

    for (let iframe of iframes) {
        if (way === 'slower') {
            var new_pb_rate = current_pb_rate - 0.1;
        } else if (way === 'faster') {
            var new_pb_rate = current_pb_rate + 0.1;
        } else if (way === 'normal') {
            var new_pb_rate = 1.0;
        }
        if (new_pb_rate > 2) {
            new_pb_rate = 2;
        } else if (new_pb_rate < 0.5) {
            new_pb_rate = 0.5;
        }
        new_pb_rate = Math.round(parseFloat(new_pb_rate) * 100) / 100;
        let player = new Vimeo.Player(iframe);
        player.setPlaybackRate(new_pb_rate);
        document.querySelector("#playback-speed").innerHTML = new_pb_rate;
        sessionStorage.setItem("playback_speed", new_pb_rate);
    }


}


$(".completed-session-mark").click(function () {
    $(this).toggleClass("checked");
    let form = this.closest('form');
    let parentDiv = form.parentNode;
    if (parentDiv.dataset.sessioncompleted) {
        parentDiv.dataset.sessioncompleted = "";
    } else {
        parentDiv.dataset.sessioncompleted = "session-completed";
    }
    set_session_as_completed(form.action, false);
});

function set_session_as_completed(session_url, force_add, coding_exercice = 'false') {
    $.ajax({
        url: session_url,
        data: {"force_add": force_add},
        type: "GET",
        success: function (json) {
            let numberOfSessionsCompleted = document.querySelector("#number-of-sessions-completed");
            let formationProgress = document.querySelector("#formation-progress");
            let percentCompleted = document.querySelector("#percent-completed");
            if (numberOfSessionsCompleted) {
                let nbrOfSessions = numberOfSessionsCompleted.innerHTML;
                let nbrOfSessionsChunks = nbrOfSessions.split(" / ");
                console.log(nbrOfSessionsChunks);
                if (nbrOfSessionsChunks.length === 2) {
                    let sessionsCompleted = Number(nbrOfSessionsChunks[0]) + json['Add'];
                    numberOfSessionsCompleted.innerHTML = `${sessionsCompleted} / ${nbrOfSessionsChunks[1]}`;
                    formationProgress.style.width = `${(sessionsCompleted / Number(nbrOfSessionsChunks[1])) * 100}%`;
                    percentCompleted.innerHTML = `${Math.ceil((sessionsCompleted / Number(nbrOfSessionsChunks[1])) * 100)}`;
                    toastr.options.timeOut = 1000;
                    if (json["Add"] === 0 || json["Add"] === 1) {
                        toastr.success('Session complétée !');
                    }
                }
            }
        },
        error: function (xhr, errmsg, err) {
        }
    });
};


function set_code_correct_or_not(exercice_pk, session_pk, code_correct) {
    $.ajax({
        url: "/formations/set_code_correct_status/" + code_correct,
        data: {"exercice_pk": exercice_pk, "session_pk": session_pk},
        type: "POST",
        success: function (json) {
            if (json['Success'] === true) {

            } else {

            }
        },
        error: function (xhr, errmsg, err) {

        }
    });
};


function check_quiz_answer() {
    var quiz_answer_checkbox = $("input[name='quiz_answers']:checked");
    var quiz_answer_id = quiz_answer_checkbox.attr('id');
    $('#' + quiz_answer_id + '_alert').removeClass("d-none");
    if (quiz_answer_checkbox.val() === 'correct') {
        $('#next-quiz-question').removeClass("d-none");
        $('#redo-quiz').removeClass("d-none");
        $('#check-quiz-answer').hide();

        $("input[name='quiz_answers']").each(function () {
            $(this).attr('disabled', true);
        })

    } else {
        quiz_answer_checkbox.attr('disabled', true);
    }
}

function check_quiz_answers_in_formation(number_of_questions) {
    var missing_answers = check_for_missing_answers(number_of_questions);
    if (missing_answers) {
        alert("Vous n'avez pas répondu à la question " + missing_answers);
        return false;
    }
    var number_of_correct_answers = 0
    for (let i = 1; i < number_of_questions + 1; i++) {
        var quiz_answers = document.querySelectorAll("input[name='quiz_answers" + i + "']");
        for (let quiz_answer of quiz_answers) {
            if (quiz_answer.checked) {
                $('#' + quiz_answer.id + '_alert').removeClass("d-none");
                quiz_answer.setAttribute('disabled', true)
                if (quiz_answer.value === 'correct') {
                    number_of_correct_answers++;
                    quiz_answer.parentElement.classList.add('border', 'border-primary');
                } else {
                    quiz_answer.parentElement.classList.add('border', 'border-danger');
                }
            }
        }
    }
    var success_text = document.querySelector('#quiz-success-text');
    var success_percentage = number_of_correct_answers / number_of_questions;
    success_text.innerHTML = "Score final : " + number_of_correct_answers + " / " + number_of_questions + " <br>";
    if (success_percentage < 0.5) {
        success_text.innerHTML += "Pas mal, mais il y a encore beaucoup de notions qui vous échappent ! Prenez le temps de bien regarder vos erreurs !";
    } else if (success_percentage < 0.8) {
        success_text.innerHTML += "Encore des erreurs, mais vous êtes sur la bonne voie !";
    } else if (success_percentage === 1) {
        success_text.innerHTML += "Bravo, c'est un sans faute ! Vous pouvez continuer l'esprit tranquille 😌";
    }
    $('#quizFinished').modal('show');

    // Complete session status
    var form = $("#complete-session-form");
    if (form) {
        set_session_as_completed(form.attr('action'), true);
        var form_div = form.children('div');
        if (form_div.length === 1) {
            checkark_div = form_div[0];
            checkark_div.classList.add("checked");
        }
    }
}

function check_for_missing_answers(number_of_questions) {
    for (let i = 1; i < number_of_questions + 1; i++) {
        var quiz_answers = document.querySelectorAll("input[name='quiz_answers" + i + "']");
        var answered = [];
        for (let quiz_answer of quiz_answers) {
            answered.push(quiz_answer.checked);
        }
        if (!answered.some(Boolean)) {
            return i;
        }
    }
    return false;
}

function navigate_session(way) {

    let session;
    if (way === 'next') {
        session = document.querySelector("#next-session");
    } else if (way === 'previous') {
        session = document.querySelector("#previous-session");
    }
    if (!session.href.endsWith("None")) {
        window.location.href = session.href;
    }
};


function openNav() {
    document.getElementById("sidebar").style.width = "600px";
    document.getElementById("openNavBar").classList.add("d-none");
    sessionStorage.setItem("navbar_hidden", 'false');
}

function closeNav() {
    document.getElementById("sidebar").style.width = "0px";
    document.getElementById("openNavBar").classList.remove("d-none");
    sessionStorage.setItem("navbar_hidden", 'true');
}


$('.quiz-answer').on('click', function () {
    var checkbox = $(this).children('input[type="radio"]');
    checkbox.prop('checked', true);
});

// EDIT USER NOTES ON SESSION
$(document).ready(function () {

    $('#user-notes').keyup(delay(function (e) {
        save_user_note();
        $('#user-notes-content').html(DOMPurify.sanitize(marked($(this).val())))
    }, 250));
});

function save_user_note() {
    var url = $('#session-url').val();
    var note_content = $('#user-notes').val();
    save_user_notes(url, note_content);
}


function save_user_notes(session_url, note_content) {
    $.ajax({
        url: session_url,
        data: {"note_content": note_content},
        type: "GET",
        success: function (json) {
            if (json['success'] === true) {
                $('#save-user-notes').addClass("d-none");
            }
        },
        error: function (xhr, errmsg, err) {
            //
        }
    });
};


function updateShoppingCart() {
    var formation_slug = $(this).attr('data-arg');
    var button = $(this);
    if ($(this).hasClass("shopping-cart-remove")) {
        var update_type = "remove";
    } else {
        var update_type = "add"
    }
    $.ajax({
        url: '/panier/update_cart_json/' + formation_slug + '/' + update_type,
        type: "GET",
        success: function (json) {
            if (json["success"]) {
                button.toggleClass("btn-success btn-outline-dark");
                button.toggleClass("shopping-cart-remove shopping-cart-add")
                if (json["number_of_items"] > 0) {
                    $("#shopping-cart-li").removeClass("d-none");
                } else {
                    $("#shopping-cart-li").addClass("d-none");
                }

                $("#shopping-cart-number").text(json["number_of_items"]);
                if (update_type == "add") {
                    button.find("span").text("-");
                } else {
                    button.find("span").text("+");
                }
            } else {
                console.log("Error!!");
            }
        },
        error: function (xhr, errmsg, err) {
            //
        }
    });
}


function startCheckout() {
    var shopping_cart_pk = $("#panier").val();
    $.ajax({
        url: 'get_total_' + shopping_cart_pk,
        type: "GET",
        success: function (json) {
            if (json["total_price"]) {
                var price = json["total_price"];
            } else {
                var price = 0;
            }
            var vendor_auth = json["paddle_vendor_auth"];
            var vendor_id = json["paddle_vendor_id"];
            openCheckout(price, shopping_cart_pk, vendor_auth, vendor_id);
        },
        error: function (xhr, errmsg, err) {
            //
        }
    });
}


function openCheckout(price, shopping_cart_pk, vendor_auth, vendor_id) {
    var docstring_image = document.getElementById('docstring-logo').src;
    var email = $('#user-email').val();
    var zip_code = $('#zip-code').val();
    var courses_bought = $('#courses-bought').val();
    var settings = {
        "async": true,
        "dataType": "jsonp",
        "crossDomain": true,
        "url": "https://vendors.paddle.com/api/2.0/product/generate_pay_link",
        "method": "POST",
        "headers": {"content-type": "application/x-www-form-urlencoded"},
        "data": {
            "vendor_id": vendor_id,
            "vendor_auth_code": vendor_auth,
            "customer_email": email,
            "customer_country": "FR",
            "customer_postcode": zip_code,
            "custom_message": "Plus que quelques étapes",
            "image_url": "https://www.docstring.fr/static/img/logo_square.png",
            "quantity_variable": 0,
            "title": "Formations Docstring - " + courses_bought,
            "webhook_url": "https://www.docstring.fr/panier/complete_transaction",
            "return_url": "https://www.docstring.fr/panier/confirmation?checkout={checkout_hash}",
            "passthrough": shopping_cart_pk,
            "prices[0]": "EUR:" + price
        }
    }

    $.ajax(settings).done(function (response) {
        if (response["success"] === true) {
            Paddle.Checkout.open({
                override: response["response"]["url"],
                locale: "fr"
            });
        }
    });
};


function success() {
    if (document.getElementById("reset-password-email").value === "") {
        document.getElementById('reset-password-button').disabled = true;
        document.getElementById('reset-password-button').classList.add("btn-secondary");
        document.getElementById('reset-password-button').classList.remove("btn-danger");
    } else {
        document.getElementById('reset-password-button').disabled = false;
        document.getElementById('reset-password-button').classList.remove("btn-secondary");
        document.getElementById('reset-password-button').classList.add("btn-danger");
    }
}


function startSubscribe() {
    var email = $("#user-email").val();
    var zip_code = $("#zip-code").val();
    var trim = $("#billingSwitch").prop('checked');
    if (trim) {
        var product_id = 579510;
    } else {
        var product_id = 579509;
    }

    if ($("#cupon-code").val()) {
        var coupon_code = $("#cupon-code").val();
    }

    Paddle.Checkout.open({
        product: product_id,
        coupon: coupon_code,
        email: email,
        country: "FR",
        postcode: zip_code,
        locale: "fr",
        success: "https://www.docstring.fr/le_club/confirmation"
    });
}

function updateSubscribe(cancel_url) {
    Paddle.Checkout.open({
        override: cancel_url,
        locale: "fr",
    });
}

// PROGRESS RING
var circle = document.querySelector('circle');
if (circle) {
    var radius = circle.r.baseVal.value;
    var circumference = radius * 2 * Math.PI;

    circle.style.strokeDasharray = `${circumference} ${circumference}`;
    circle.style.strokeDashoffset = `${circumference}`;
}

function setProgress(percent) {
    const offset = circumference - percent / 100 * circumference;
    circle.style.strokeDashoffset = offset;
}


// PROFILE BADGES
$('.interest-badge').click(function () {
    $(this).toggleClass("badge-dark badge-primary");
    setInterestBadgeValues();

})

function setInterestBadgeValues() {

    var interests_input = $("#interests")
    var interests = ""
    $('.interest-badge').each(function () {
        if ($(this).hasClass("badge-primary")) {
            interests = interests + "," + $(this).text();
        }
    })
    interests_input.val(interests);

}


$('.filter-badge').click(function () {
    $(this).toggleClass("badge-dark badge-primary");
    filter_sessions();
});


$("#search-session").keyup(function () {
    filter_sessions();
});

$("#search-glossary").keyup(function () {
    filter_glossary();
});

$("#search-notes").keyup(function () {
    filter_notes();
});

function filter_sessions() {
    var search_term = $("#search-session").val().toLowerCase();

    var number_of_results_el = document.querySelector("#number-of-results");
    var number_of_results = 0;
    let hide_completed_sessions;

    let filters = [];
    let badges = document.querySelectorAll(".filter-badge");

    for (let badge of badges) {
        if (badge.classList.contains("badge-primary") && badge.id !== "session-completed-filter") {
            filters.push(badge.dataset.filtertype);
        }
    }

    if (filters.length === 0) {
        for (let badge of badges) {
            filters.push(badge.dataset.filtertype);
        }
    }

    hide_completed_sessions = !!document.querySelector("#session-completed-filter").classList.contains("badge-primary");

    let sessionsSommaire = document.querySelectorAll(".session-sommaire");
    for (let sessionSommaire of sessionsSommaire) {
        let session_type = sessionSommaire.dataset.sessiontype;
        let session_name = sessionSommaire.querySelector("h5").innerText.toLowerCase();
        let session_completed = sessionSommaire.dataset.sessioncompleted === "session-completed";

        let match_type = filters.includes(session_type);
        let match_name = search_term.length > 0 ? session_name.indexOf(search_term) > 0 : true;
        let match_completed = hide_completed_sessions ? !session_completed : true;

        if ([match_type, match_name, match_completed].every(Boolean)) {
            sessionSommaire.style.opacity = "";
            number_of_results += 1;
        } else {
            sessionSommaire.style.opacity = "0.1";
        }

    }

    number_of_results_el.innerHTML = number_of_results + " session" + (number_of_results > 1 ? "s" : "")

}


function filter_glossary() {
    var search_term = $("#search-glossary").val().toLowerCase();
    var number_of_results_el = document.querySelector("#number-of-results");
    var number_of_results = 0;
    $(".glossary-item").each(function () {
        var glossary_title = $(this).data("glossarytitle").toLowerCase();
        var glossary_description = $(this).find(".glossary-description").html().toLowerCase();

        if (glossary_title.includes(search_term) || glossary_description.includes(search_term)) {
            $(this).removeClass("d-none");
            number_of_results += 1;
        } else {
            $(this).addClass("d-none");
        }
        number_of_results_el.innerHTML = number_of_results + " glossaire" + (number_of_results > 1 ? "s" : "")

    });
}

function filter_notes() {
    var search_term = $("#search-notes").val().toLowerCase();
    $(".note-header").each(function () {
        var note_title = $(this).data("notetitle").toLowerCase();
        var note_description = $(this).find("p").html().toLowerCase();

        if (note_title.includes(search_term) || note_description.includes(search_term)) {
            $(this).removeClass("d-none");
        } else {
            $(this).addClass("d-none");
        }

    });
}


function open_modal() {
    $('#PreviewCourse').modal();
    var vid = document.getElementById("background-video");
    vid.pause();

}

function toggle_favorite(el, content_type, content_pk, formation_pk = null) {
    var toggle_favorite = el.src.endsWith('favorite_off.png');

    if (el.src.endsWith('favorite_on.png')) {
        el.src = el.src.replace("_on.png", "_off.png")
    } else {
        el.src = el.src.replace("_off.png", "_on.png")
    }

    $.ajax({
        url: '/compte/add_to_favorite',
        data: {
            'content_type': content_type,
            'content_pk': content_pk,
            'formation_pk': formation_pk,
            'toggle_favorite': toggle_favorite
        },
        type: "POST",
        success: function (json) {
            toastr.options.timeOut = 1000;
            if (toggle_favorite) {
                toastr.success('Favori ajouté !');
            } else {
                toastr.error('Favori supprimé !');
            }
        },
        error: function (xhr, errmsg, err) {

        }
    });
}
