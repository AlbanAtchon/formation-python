# Importer les modules standards en premiers
import logging
import json
import os

# En 2eme les modules importés mais pas créer par mes soins

# En 3eme importer les modules propres à mon projet

from constants import DATA_DIR

LOGGER = logging.getLogger()

# On pourra utiliser les fonctionnalité de list()

class Liste(list):
    def __init__(self, nom):
        self.nom = nom

    def ajouter(self, element):
        # vérifier si l'\élément n\'est pas de type str avec insinstance()
        if not isinstance(element, str):
            # raise pour lever l\'erreur
            raise ValueError("Vous ne pouvez ajouter que des chaines de caractères !")

        if element in self:
            LOGGER.error(f"{element} est déjà dans la liste")
            return False

        self.append(element)
        return True

    def enlever(self, element):
        if element in self:
            self.remove(element)
            return True
        return False

    def afficher(self):
        print(f"Ma liste de {self.nom} :")
        for element in self:
            print(f" --> {element}")

    def sauvegarder(self):
        chemin = os.path.join(DATA_DIR, f"{self.nom}.json")
        # Vérification de l\'existance du fichier
        if not os.path.exists(DATA_DIR):
            os.makedirs(DATA_DIR)

        with open(chemin, "w") as f:
            json.dump(self, f, indent=4)
        return True

if __name__ == "__main__":
    liste = Liste("Courses")
    liste.ajouter("Pommes")
    liste.ajouter("Poires")
    liste.afficher()
    liste.sauvegarder()


