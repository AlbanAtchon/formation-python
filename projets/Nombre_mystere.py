## Jeu du nombre mystère ###

import random
import os


### Correction

from random import randint

number_to_find = randint(0, 100)
remaining_attempts = 5

print("*** Le jeu du nombre mystère ***")

# Boucle principale
while remaining_attempts > 0:
    print(f"Il te reste {remaining_attempts} essai{'s' if remaining_attempts > 1 else ''}")

    # Saisie de l'utilisateur
    user_choice = input("Devine le nombre : ")
    if not user_choice.isdigit():
        print("Veuillez entrer un nombre valide.")
        continue
    
    user_choice = int(user_choice)    

    if number_to_find > user_choice:  # Plus grand
        print(f"Le nombre mystère est plus grand que {user_choice}")
    elif number_to_find < user_choice:  # Plus petit
        print(f"Le nombre mystère est plus petit que {user_choice}")
    else:  # Égal (succès)
        break

    remaining_attempts -= 1

# Gagné ou perdu
if remaining_attempts == 0:
    print(f"Dommage ! Le nombre mystère était {number_to_find}")
else:
    print(f"Bravo ! Le nombre mystère était bien {number_to_find} !")
    print(f"Tu as trouvé le nombre en {6 - remaining_attempts} essai")

print("Fin du jeu.")


'''
print(" *-* Le jeu du nombre mystère *-* ")
print(" Trouver le nombre mystère en 0 et 100")

print("--" * 20)
print(" Il vous reste 5 essais ")

## Génération du nombre aléatoire entre 0 et 100 et entrée du user

nbr_mys = random.randint(0,100)

# Initialisation des nombres d'essais réalisés et des essais restant

nbr_rest = 5
nbr_essai = 1
nbr_total = 5

while True : 

    nbr = input(" Devine le nombre : ")

    # Vérifiaction de l'input : qu'il n'est pas un nombre
    if not nbr.isdigit() :
        print("Veillez entrer un nombre valide")
        print(f" Il vous reste {nbr_rest} essais ")
        continue

    #  l'input est un nombre et le nombre d'essais est inférieur à 5
    if nbr.isdigit() and nbr_rest < 0 :
    
        # Condition où le nombre mystère est supérieur au nombre entrée
        if nbr_mys > int(nbr) :
            nbr_rest = nbr_rest - 1
            nbr_essai = nbr_essai + 1
            print(f" Le nombre mystère est plus grand que {nbr}")
            print(f" Il vous reste {nbr_rest} essais")
            
        # Condition où le nombre mystère est inférieur au nombre entrée 
        elif nbr_mys < int(nbr) : 
            nbr_rest = nbr_rest - 1
            nbr_essai = nbr_essai + 1
            print(f" Le nombre mystère est plus petit que {nbr}")
            print(f" Il vous reste {nbr_rest} essais")

        # Condition où le nombre mystère est égal au nombre entrée 
        elif nbr_mys == int(nbr) :
            print("~~" * 10)
            print(f" Bravo! Le nombre mystère était bien {nbr_mys} ! ")
            print(f" Tu as trouvé le nombre en {nbr_essai} essais ")
            break
    # Aucune des condidtions n'est vérifiée après 5 essais
    else:
        print(f" Dommage! Le nombre mystère était {nbr_mys}")
        break
        
        
'''
