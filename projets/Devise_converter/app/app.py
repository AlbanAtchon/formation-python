from PySide2 import QtWidgets
import currency_converter

# Respecter l'ordre de création des d'ajout des valurs par défauts et autres fonctionnalités

class App(QtWidgets.QWidget): # Permet d'utiliser tt toutes les fonctionnalités de QWidgets dans notre App()
    def __init__(self):
        super().__init__()                                      # super() permet d'acceder à QtWidgets.QWidget dont la class hérite
        self.c = currency_converter.CurrencyConverter()         # Récupérer les devises possible à la conversion
        self.setWindowTitle("Convertisseur de devises")         # Non de la fenetre
        self.setup_ui()
        self.set_default_values()
        self.setup_connections()
        self.setup_css()
        
        
    def setup_ui(self):
        self.layouts = QtWidgets.QHBoxLayout(self)                      # création d'une boxe layout horizontal
        self.cbb_devisesFrom = QtWidgets.QComboBox()                    # création d'un combo boxe 
        self.spn_montant = QtWidgets.QSpinBox()                         # boxe avec des valeurs numéiques >= 0
        self.cbb_devisesTo = QtWidgets.QComboBox()                      # boxe vide avec option de liste déroulante
        self.spn_montantConverti = QtWidgets.QSpinBox()                 #
        self.btn_inverser = QtWidgets.QPushButton("Inverser devises")   # Bouton inversion
        
        # Ajouter les widgets dans le layout
        self.layouts.addWidget(self.cbb_devisesFrom)
        self.layouts.addWidget(self.spn_montant)
        self.layouts.addWidget(self.cbb_devisesTo)
        self.layouts.addWidget(self.spn_montantConverti)
        self.layouts.addWidget(self.btn_inverser)

    def set_default_values(self):
        self.cbb_devisesFrom.addItems(sorted(list(self.c.currencies)))   # Ajouter d'élément à l'intérieur des combo box, qui doivent être des strings
        self.cbb_devisesTo.addItems(sorted(list(self.c.currencies)))            
        self.cbb_devisesFrom.setCurrentText("EUR")                       # Définir la devise qui apparait par défaut
        self.cbb_devisesTo.setCurrentText("EUR")

        self.spn_montant.setRange(1, 100000000)                          # Intervalle possbile de convertion
        self.spn_montantConverti.setRange(1, 100000000)

        self.spn_montant.setValue(100)                                   # Définir un montant par défaut à convertir
        self.spn_montantConverti.setValue(100)
        
    def setup_connections(self):
        self.cbb_devisesFrom.activated.connect(self.compute)             # activated correspond au signal qu'on connecte à notre methode avec la méthode connect(), et on exécute compute à chaque fois que cette methode est appelé
        self.cbb_devisesTo.activated.connect(self.compute)
        self.spn_montant.valueChanged.connect(self.compute)              # pour le changement de valeur à convertir
        self.btn_inverser.clicked.connect(self.inverse_devise)              
    
    def setup_css(self):
        self.setStyleSheet("""
        background-color: rgb(30, 30, 30);
        color: rgb(240, 240, 240);
        border: None;                   
        """)
        
        
    def compute(self):
        montant = self.spn_montant.value()                               # récupération du montant entrée 
        devise_from = self.cbb_devisesFrom.currentText()
        devise_to = self.cbb_devisesTo.currentText()
        try:
            resultat = self.c.convert(montant, devise_from, devise_to)
        except currency_converter.currency_converter.RateNotFoundError: 
            print("La conversion n'a pas fonctionné.")
        else:
            self.spn_montantConverti.setValue(resultat)
            
    
    def inverse_devise(self):
        devise_from = self.cbb_devisesFrom.currentText()
        devise_to = self.cbb_devisesTo.currentText()
        
        self.cbb_devisesFrom.setCurrentText(devise_to)
        self.cbb_devisesTo.setCurrentText(devise_from)
        
        self.compute()
        
        


app = QtWidgets.QApplication([])  # créer une qapplication et lancer avec exec_ 
win = App()  # pour afficher la fenetre 
win.show()   # pour afficher la fenetre à l'écran
app.exec_()  # Exécution de l'application