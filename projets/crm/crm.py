
import re
import string

from dataclasses import dataclass
from textwrap import indent
from tinydb import TinyDB, where
from pathlib import Path
from typing import List

@dataclass  # Remplace ma méthode __init__
class User:
    
    DB = TinyDB(Path(__file__).resolve().parent / 'db.json', indent=4)
    
    first_name: str
    last_name: str
    phone_number: str = ""
    address: str = ""
    
    def __repr__(self): # Pour avoir une représentation de la class
        return f"User({self.first_name}, {self.last_name})"

    def __str__(self):
        return f"{self.full_name}\n{self.phone_number}\n{self.address}"
    
    @property # permettre que les changements de noms et prenoms soit dynamique
    def full_name(self):
        return f"{self.first_name} {self.last_name}"
    
    @property
    def db_instance(self):
        return User.DB.get((where('first_name') == self.first_name) & (where('last_name') == self.last_name))
    
    def _check_phone_numbrer(self) :              # Mettre un seul triret '_' au début du nom de la méthode, permet de spécifier que cette méthode ne doit pas être utilisé directement
        phone_number = re.sub(r"[+()\s]*", "", self.phone_number)       # rechercher plusieurs fois le groupe de caractère entre [] et remplacer par "" avec sub, dans phone_number
        if len(phone_number) < 10 or not phone_number.isdigit():
            raise ValueError(f"Numéro de téléphone {self.phone_number} invalide.") # Lever l'erreur si l'un des conditions est vérifiée
    
    def _check_names(self):
        if not(self.first_name and self.last_name):
            raise ValueError("Le prénom et le nom de famille ne peuveut pas être vides.")
        special_caracteres = string.punctuation + string.digits             # chaine de caractère contenant les caratères de ponctuation et les chiffres
        for character in self.first_name + self.last_name:
            if character in special_caracteres:
                raise ValueError(f"Nom invalide {self.last_name}")
                     
    def _checks(self):
        self._check_phone_numbrer
        self._check_names
     
    def exists(self):  # Vérification l'utilisateur existe dans le base de données
        return bool(self.db_instance)
       
    def delete(self) -> list[int]:
        if self.exists():
            return User.DB.remove(doc_ids=[self.db_instance.doc_id])
        return []
        
    def save(self, validate_data: bool=False) -> int:
        if validate_data:
            self._checks()
        return User.DB.insert(self.__dict__) # Récupère les attributs de notre instance dans un dico avec __dict__ et l'insert dans la base de données


def get_all_users():
    return [User(**user) for user in User.DB .all()] # Récupère chaque dico et on recrée les instances via User à grâce à la base de données

  
    
if __name__ == "__main__":
    martin = User("Gabrielle", "Martin")
    print(martin.db_instance)
    print(martin.exists())
    print(martin.delete())
    #print(get_all_users())
    # from faker import Faker
    # fake = Faker(locale="fr_FR")
    # for _ in range(10) : 
    #     user = User(first_name = fake.first_name(), 
    #                 last_name = fake.last_name(), 
    #                 phone_number = fake.phone_number(), 
    #                 address = fake.address())
    #     print(user.save())
    #     print("-" * 10)