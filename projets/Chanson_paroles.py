
# WEB SCRAPING

# Pour vérifier les mots les plus utiliser par un chanteur connu
# - Aller sur le site Genius: Les chansons de Patrick bruel
# - Récupérer l'adresse url de la page1 dans le code source de la page
import json

from bs4 import BeautifulSoup
from pprint import pprint

import requests
import collections
import json


def is_valid(word):
    return "[" not in word and "]" not in word


def extract_lyrics(url):
    print(f"Fetching lyrics {url}...")
    r = requests.get(url)
    if r.status_code != 200:
        return []

    soup = BeautifulSoup(r.content, 'html.parser')
    lyrics = soup.find("div", class_="Lyrics__Container-sc-1ynbvzw-8 eOLwDW")
    if not lyrics:
        return extract_lyrics(url)

    all_word = []
    sentence_words = []
    for sentence in lyrics.stripped_strings:
        # sentence_words = [word.strip(",").strip(".").strip("?").lower() for word in sentence.split() if len(word) >
        # word_length and "[" not in word and "]" in word]
        sentence_words = [(word.strip(",").strip(".").lower()) for word in sentence.split() if is_valid(word)]
        all_word.extend(sentence_words)

    return all_word


def get_all_urls():

    page_number = 1
    links = []
    while True:
        r = requests.get(f"https://genius.com/api/artists/29743/songs?page={page_number}&sort=popularity")
        # print(r.status_code)
        if r.status_code == 200:
            print(f"Fetching page {page_number}")
            # récupération de response et next_page avec get()
            response = r.json().get("response", {})
            next_page = response.get("next_page")

            # récupération des url de chaque son avec une compréhension de liste en rajoutant avec extend url trouvé
            songs = response.get("songs")
            links.extend([song.get("url") for song in songs])
            page_number += 1

            if not next_page:
                print("No more pages to fetch.")
                break
    return links


def get_all_words():
    # urls = get_all_urls()
    # print(urls)
    # words = []
    # for url in urls:
    #     lyrics = extract_lyrics(url=url)
    #     words.extend(lyrics)
    #
    #     with open("data.json", "w") as f:
    #         json.dump(words, f, indent=4)

    with open("data.json", "r") as f:
        words = json.load(f)

    counter = collections.Counter([w for w in words if len(w) > 4])
    most_common_words = counter.most_common(15)
    pprint(most_common_words)


get_all_words()