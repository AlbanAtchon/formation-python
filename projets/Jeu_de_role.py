

# Auteur : Alban Atchon et Carine Gnide
# Date: 24-07-2021
# Rôle : Jeu de rôle formation python

import random
import os
import emoji
from emoji import emojize
from random import randint

# variables définitions

my_pts = 50
enm_pts = 50
potion = 3


while my_pts >= 0 and enm_pts >= 0 :

    choix = input("Souhaitez vous attaquer [1] ou utiliser une potion [2] ? Choisir 1 ou 2 : ")

    if not choix.isdigit():
        continue

    if int(choix) == 1 :
        dg_my = randint(5,10)
        dg_enm = randint(5,15)
        my_pts = my_pts - dg_enm
        enm_pts = enm_pts - dg_my

        if my_pts <= 0 and enm_pts > 0 :
            print("Tu as perdu \U0001F923")
            print("Fin du jeu \U0001F3C1")
            break

        elif enm_pts <= 0 and my_pts > 0 :
            print("You are the Winner \U0001F44F \U0001F3C6")
            print("The end \U0001F3C1")
            break

        else: 
            print(f" L'ennemi vous a infligé {dg_enm} points de dégats \U0001F52B ")
            print(f" Il vous reste {my_pts} points de vie")
            print(f" Vous avez infligé {dg_my} points de dégats à l'ennemi \U0001F52B ")
            print(f" Il reste à l'ennemi {enm_pts} points de vie ")
        
    
        print("--"*30)

    elif int(choix) == 2 and int(potion) > 0 :
        dg_my = randint(5,10)
        dg_enm = randint(5,15)
        pt_potion = randint(15,50)

        potion = potion - 1
        my_pts = my_pts + pt_potion
        my_pts = my_pts - dg_enm

        print(f" Vous récupérez {pt_potion} de vie \U00002764 ( {potion} \U0001F9EA restantes) ")
        print(f" L'ennemi vous a infligé {dg_enm} points de dégats \U0001F52B ")
        print(f" Il vous reste {my_pts} points de vie")
        print(f" Il reste à l'ennemi {enm_pts} points de vie")

        print("--"*30)

        dg_enm = randint(5,15)
        my_pts = my_pts - dg_enm

        print(" Vous passez votre tour \U0001F4DB")
        print(f" L'ennemi vous a infligé {dg_enm} points de dégats \U0001F52B ")
        print(f" Il vous reste {my_pts} points de vie")

        print("--"*30)

    elif int(choix) == 2 and int(potion) == 0 :
        print("Vous ne disposez plus de potion")

    else:
        choix = input("Souhaitez vous attaquer (1) ou utiliser une potion (2) ? Choisir 1 ou 2 : ")

