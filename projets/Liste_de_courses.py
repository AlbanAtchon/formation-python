 
### Projet liste de course ###
## Ajout de la sauvegarde automatique de la liste de course 25/0/2021

import os
import json

continuer = True

menu_titre = "Choisissez parmi les 5 options suivantes :" 
option_1 = "1: Ajouter un élément à la liste"
option_2 = "2: Retirer un élément de la liste"
option_3 = "3: Afficher la liste"
option_4 = "4: Vider la liste"
option_5 = "5: Quitter"


menu = '\n' + menu_titre + '\n' + option_1 + '\n' + option_2 + '\n' + option_3 + '\n' + option_4 + '\n' + option_5
print(menu)

save_liste = "save.json"
#CUR_DIR = os.path.dirname(os.path.abspath(__file__))
JSON_FILE = os.path.split(os.path.realpath(__file__))[0] + '/' + save_liste


choix = list(range(1,6))

#choix = [1, 2, 3, 4, 5]
liste_course = []

# Charger la liste de course si elle exite
if os.path.exists(JSON_FILE) :
    with open(JSON_FILE, "r") as jfile:
        liste_course = json.load(jfile)


with open(JSON_FILE, "w") as jfile:

    while continuer :
        le_choix = input("==> Votre choix : ")
        if not le_choix.isdigit() :
            print("Veuillez choisir une option valide ! ")
            continue
        
        if int(le_choix) not in choix :
            print("Veuillez choisir une option valide ! ")
            continue

        ## Ajout d'un élément à la liste
        if int(le_choix) == 1 :
            ajout = input("Entrer le nom d'un élément à ajouter à la liste de courses : ")
            liste_course.append(ajout)
            print(f"L'élément {ajout} a bien été ajouté à la liste")
            
        ## Retrait d'un élément de la liste
        elif int(le_choix) == 2 :
            retrait = input("Entrer le nom de l'élément à retirer de la liste de courses : ")
            if retrait in liste_course:
                liste_course.remove(retrait)
                print(f"L'élément {retrait} a bien été supprimé de la liste")
            else:
                print(f"L'élément {retrait} n'est pas présent dans la liste")

        ## Lister le contenu de la liste 
        elif int(le_choix) == 3 :
            if liste_course:
                print("Voici votre liste de courses : ")
                for elmt, present in enumerate(liste_course, 1) :
                    print(f"{elmt}. {present}")
            else:
                print("Votre liste ne contient aucun élément")

        ## Nettoyer le contenu de la liste       
        elif int(le_choix) == 4 :
            liste_course.clear()
            print("La liste de course a été entièrement néttoyée")

        ## Quitter le programme et sauvegarder la liste de course finale
        elif int(le_choix) == 5 :
            json.dump(liste_course, jfile, indent=4)
            print(" Au revoir et à bientôt ^o^ ")
            exit()
        print( "==" * 25)


# Correction du projet 