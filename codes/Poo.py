class Voiture:
    # des variables = ATTRIBUTS de classe
    # marque = "Lamborghini"
    # couleur = "blue"
    voiture_crees = 0

    # Initialisation d'attribut instance (méthode __init__) qui correspond à une fonction
    def __init__(self, marque):
        Voiture.voiture_crees += 1
        self.marque = marque  # self.attribut = valeur , pour définir un attribut d'instance


# Instances de la classe
voiture_1 = Voiture("Lamborghini")
voiture_2 = Voiture("Porsche")

print(Voiture.voiture_crees)


# Voiture.marque ="Porsche" # va modifier la marque dans la classe voiture et donc les instances créées

# print(voiture_2.marque)

# Modification de la marque des deux instances
# voiture_1.marque = "Peugeot"
# voiture_2.marque = "Toyota"

# print(Voiture.marque)
# print(voiture_1.marque)
# print(voiture_2.marque)

class Livre:
    def __init__(self, nom, nombre_de_pages, prix):
        self.nom = nom
        self.nombre_de_pages = nombre_de_pages
        self.prix = prix
    def afficher_info(self):
        print(f'Il s\'agit du livre d\'{self.nom} au prix de {self.prix} l\'unité')

livre_HP = Livre("Harry Potter", 300, 10.99)
livre_LOTR = Livre("Le Seigneur des Anneaux", 400, 13.99)

livre_HP.afficher_info()