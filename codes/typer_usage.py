
import typer
import time

app = typer.Typer()

def main(delete: bool = typer.Option(False, help="Supprime les fichiers trouvés"), 
         extensions: str = typer.Argument("txt", help="Extension à chercher")):
    """
    Affiche les fichiers trouvés avec l'extension données.
    
    """
    
    typer.echo(f"Recherche des fichiers avec les {extensions}.")
    #extension = typer.prompt("Quelle extension souhaitez-vous chercher ?")
    if delete:
        confirm = typer.confirm("Souhaitez vous vraiment supprimer les fichiers ?") # ajouter dans about=True si on n'a pas besoin de la condition
        if not confirm:
            #note = typer.style("On annule l'opération.", bold=True, fg=typer.colors.RED, bg=typer.colors.WHITE)
            typer.secho("On annule l'opération.", bold=True, fg=typer.colors.RED)
            raise typer.Abort()

        typer.echo("Suppression des fichiers.")
        
    prenoms = range(100)
    with typer.progressbar(prenoms) as progress:
        for prenom in prenoms:
            time.sleep(0.05)
        
    typer.echo("Fin du script")

@app.command()
def search_py():
    main(delete=False, extensions="py")

@app.command()
def delete_py():
    main(delete=True, extensions="py")
    
    
if __name__ == "__main__":
    #typer.run(main)
    app()