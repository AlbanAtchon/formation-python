"""
Trier les fichiers contenus dans le dossier data selon les associations suivantes :
mp3, wav, flac : Musique
avi, mp4, gif : Videos
bmp, png, jpg : Images
txt, pptx, csv, xls, odp, pages : Documents
autres : Divers
"""

import pathlib
from pathlib import Path

dossiers = {'.mp3': 'Musique', '.wav':'Musique', '.flac':'Musique',
            '.avi':'Videos', '.mp4':'Videos', '.gif':'Videos',
            '.bmp':'Images', '.png':'Images', '.jpg':'Images',
            '.txt':'Documents', '.pptx':'Documents', '.csv':'Documents', '.xls':'Documents', '.odp':'Documents', '.pages':'Documents'

}

data_dir = Path.cwd() / "data"

fichiers = [files for files in data_dir.iterdir() if files.is_file()]

for files in fichiers:
        nv_dossier = data_dir / dossiers.get(files.suffix, 'Divers')
        nv_dossier.mkdir(exist_ok=True)
        files.rename(nv_dossier /files.name)
        #print(nv_dossier)


def add(a: int, b: int) -> int:
        return a + b

add(1,1)