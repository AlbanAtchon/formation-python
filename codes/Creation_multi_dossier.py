import pathlib
from pathlib import Path

chemin = Path.cwd() / "Projet_formation"
 
d = {"Films": ["Le seigneur des anneaux",
               "Harry Potter",
               "Moon",
               "Forrest Gump"],
     "Employes": ["Paul",
                  "Pierre",
                  "Marie"],
     "Exercices": ["les_variables",
                   "les_fichiers",
                   "les_boucles"]}


for key,val in d.items():
    niveau_1 = chemin / key
    #print(niveau_1)
    for j in val:
        niveau_2 = niveau_1 / j
        niveau_2.mkdir(parents=True, exist_ok=True)