import logging

# par défaut le niveau est à warning
logging.basicConfig(level=logging.DEBUG,
                    filename="log_file.log",
                    filemode="w",
                    format='%(asctime)s - %(levelname)s : %(message)s')



logging.debug("La fonction a bien été exécutée")
logging.info("Message d'information")
logging.warning("Attention !")
logging.error("Une erreur est arrivée")
