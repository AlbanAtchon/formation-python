### FORMATION PYTHON UDEMY ####
### 6 Janvier 2021 ###

# Les chaines de caractères multilignes avec 3 """
# les chaines de caractères brutes ou 'raw string'. EX : un chemin vers un dossier dont le nom contient \n ou \t, utilisation de r'ici_chaine_de_caratère'

#print(r'/home/alban/Documents/notre_exemple/time') # si non \t et \n seront interprétés comme saut de ligne et tabulation

# Possibilité d'additionné des booléens et des nombres

## les objets natifs 

# Créer un objet de type chaîne de caractères
chaine = "ceci est une chiane de caractère"
# Créer un objet de type nombre entier
nombre_entier = int(10)
# Créer objet de type nombre décimal
nombre_decimal = int(10.2)
# Créer un objet de type booléen
boolean = True

# fonction id() récupère la place en mémoire d'un objet
# affection parallele ex: a,b = 12,2 ; a,b = b,a

## Fonction 'input()'
## Focntion "chaine".upper() change la chaine en majuscule
# et 'chaine'.lower() change en minuscule
# .capitaliez() change la première lettre en majuscule
# .title() change la 1ere lettre de chaque mot en majuscule

## les opérateurs spéciaux
# modulo '%' récupère ce qui reste d'une divsion
# la division entière '//' récupère un nombre entier
# puissance '**' 

##  Concaténation
# les f-string,
# méthode format
# explication complémentaire : https://www.docstring.fr/blog/le-formatage-des-chaines-de-caracteres-avec-python/

## Projet 1 , calculatrice pour deux nombres

## les modules
# le module random avec sa fonction randint(), uniform() ,randrange()

import random

a = random.randint(1, 7)
b = random.randint(2, 10)


if b > a :
    print("Le nombre b est plus grand que le nombre a")

elif a > b :
    print("Le nombre a est plus grand que le nombre b")
    
elif a == b :
    print("Le nombre a et le nombre b sont égaux")