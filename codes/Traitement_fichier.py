
with open("prenoms.txt", "r") as prenoms:

    first_name = prenoms.read().splitlines()

nv_liste = []
for name in first_name:
    name = name.split()
    nv_liste.extend(name)

nv_prenom = [nv.strip(",. ") for nv in nv_liste]
nv_prenom = sorted(nv_prenom)

with open("Prenoms_propre.txt", "w") as file:
    for name in nv_prenom:
        file.write(name)
        file.write("\n")


"""
Correction 

with open("/Users/thibh/Documents/prenoms.txt", "r") as f:
    lines = f.read().splitlines()

prenoms = []
for line in lines:
    prenoms.extend(line.split())

prenoms_final = [prenom.strip(",. ") for prenom in prenoms]

with open("/Users/thibh/Documents/prenoms_final.txt", "w") as f:
    f.write("\n".join(sorted(prenoms_final)))
"""