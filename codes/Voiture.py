
class Voiture:
    def __init__(self):
        self.essence = 100

    def afficher_reservoir(self):
        print(f'La voiture contient {self.essence} litres d\'essence')

    def roule(self, kilometre=int()):
        if self.essence <= 0:
            print("Vous n\'avez plus d\'essence, faites le plein !")
            return # arrête la méthode

        self.essence -= (kilometre * 5)/100

        if self.essence < 10:
            print('Vous n\'avez bientôt plus d\'essences')

        # self.afficher_reservoir()
        print(f'La voiture contient {self.essence} litres d\'essence')

    def faire_le_plein(self):
        self.essence = 100
        print("Vous pouvez repartir !")

