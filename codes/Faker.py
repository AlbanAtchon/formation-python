
from faker import Faker

fake = Faker(locale="fr_FR")

# print(fake.text())
# print(fake.name())

numbers = [fake.unique.random_int() for _ in range(500)]
assert len(numbers) == len(set(numbers)) # ne lèvera jamais de AssertionError

# for _ in range(10):
#     print(fake.job())
#     print(fake.file_path(depth=5, category="video"))
#     print(fake.credit_card_number(), fake.credit_card_expire(), fake.credit_card_security_code())
#     print(fake.rgb_color())
#     print(fake.hex_color())


# Générer les données isbn (identifiant) pour les livres

for _ in range(5):
    print(fake.numerify(text="%%%-#-%%%%-%%%%-%%%-##"))
    print(fake.bothify(text="Product NUmber: ????-########"))