file = input("Entrez un fichie à ouvrir :")

try:
	f = open(file,'r')
	print(f.read())

except FileNotFoundError :
	print("Le fichier est introuvable")
	
except UnicodeDecodeError :
	print("Impossible d'ouvir le fichier")

else:
	f.close()



