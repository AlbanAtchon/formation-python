### Vérification de mot de passe ###
### 07/01/2021 ###
### Alban K. ATCHON ####

mdp = input("Entrez un mot de passe (min 8 caractères) : ")
mdp_trop_court = "votre mot de passe est trop court."

mdp_nbr = " Votre mot de passe ne contient que des nombres"
mdp_ok = "Inscription terminée"

if mdp == "":
    invalide = mdp_trop_court.upper()
    print(invalide)
elif len(mdp) < 8 :
    insuffisant = mdp_trop_court.capitalize()
    print (insuffisant)
elif mdp.isdigit():
    print(mdp_nbr)
else:
    print(mdp_ok)

''' Correction de la formation
mdp = input("Entrez un mot de passe (min 8 caractères) : ")
mdp_trop_court = "votre mot de passe est trop court."

if len(mdp) == 0:
    print(mdp_trop_court.upper())
    exit()
elif len(mdp) < 8:
    print(mdp_trop_court.capitalize())
    exit()

if mdp.isdigit():
    print("Votre mot de passe ne contient que des nombres.")
    exit()

print("Inscription terminée.")
'''    